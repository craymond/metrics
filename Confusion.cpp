	
#include "Confusion.hpp"
namespace metrics{

void Confusion::add(cuint i, cuint j)
{
		++confusion[i][j];
		++somme;
}

reel Confusion::nbCorrects() const
{
//sun up the diagional
	//return std::accumulate(confusion.begin(),confusion.end(),0,[](creel c,const auto& l){static uint i=0;return c+l[i++];});
	reel som=0;
	for(uint i=0;i<confusion.size();++i)
		som+=confusion[i][i];
	return som;
}

Confusion& Confusion::operator+=(const Confusion& d)
{
	somme+=d.somme;
	for(uint i=0;i<confusion.size();++i)
		std::transform(confusion[i].cbegin(),confusion[i].cend(),d.confusion[i].cbegin(),confusion[i].begin(), std::plus<reel>());	
	return *this;
}

void Confusion::format_labelnames(std::vector<string>& labels_name) const
{
	if(labels_name.size() != confusion.size())
	{
		labels_name.resize(confusion.size());
		for(uint i=0;i<labels_name.size();++i)
			labels_name[i]=std::to_string(i);
	}
	else
	{
		for(uint i=0;i<labels_name.size();++i)
			labels_name[i]=std::format("{}:{}",i,labels_name[i]);
	}	
}

string Confusion::getAsText(std::vector<std::string> labels_name) const
{
	this->format_labelnames(labels_name);
	asciitablestream tab;	
	tab << "classified as ->" ;
	tab << labels_name << asciitablestream::endl;
	for (uint i = 0; i < confusion.size(); ++i)
		tab << labels_name[i] << confusion[i] << asciitablestream::endl;		

	return "Confusion matrix:\n"+tab.toString();
}

string Confusion::getAsHtml(std::vector<std::string> labels_name) const
{
	this->format_labelnames(labels_name);
	std::ostringstream html;
	html << "<table CELLPADDING=6 align=center BORDER=2 BORDERCOLOR=black>\n";
	html << "<CAPTION>Confusion matrix</CAPTION>\n";

	html << "<tr align=center><td>classified as &#x2192;</td>";
	for (uint i = 0; i<confusion.size(); ++i)
		html << "<td>" << i << "</td>";

	html << "</tr>\n";

	for (uint i = 0; i<confusion.size(); ++i)
	{
		html << "<tr align=center><td>" << labels_name[i] << "</td>";
		for (uint j = 0; j< confusion[i].size(); ++j)
		{
			if (i == j)
				html << std::format("<td bgcolor=green>{}</td>",confusion[i][j]);
			else
				html << std::format("<td bgcolor=#FF{}FF>{}</td>",color(i, j),confusion[i][j] );

		}
		html << "</tr>\n";
	}

	html << "</table>\n";
	return html.str();
}


string Confusion::color(cuint un, cuint deux) const
{
	std::ostringstream out;
	if(confusion.at(un)[deux]==0) return "FF";
	out << std::hex <<static_cast<uint>(255-255*pow(confusion[un][deux]/(confusion[deux][un]+confusion[un][un]+confusion[deux][deux]),0.4));
//	cerr << out.str()<< endl;
	return out.str();
}


void Confusion::reset()
{
	for(auto& e: confusion)
		e.assign(e.size(),0);
}



Confusion::Confusion(cuint nbClasses, creel init): confusion(nbClasses,std::vector<reel>(nbClasses,init))
{

}

}

