	
#include "Metrics.hpp"

namespace metrics
{


classificationReport::classificationReport(cuint nbLabels): _reference(nbLabels,0),_prediction(nbLabels,0), _confusion(nbLabels), _standartMetrics(nbLabels+1)
{

}

bool classificationReport::add(cuint refe, cuint predict,creel weight)
{
	++_nbSamples;
	_confusion.add(refe,predict);
	_reference[refe]+=weight;
	_prediction[predict]+=weight;
	_total_pred+=weight;
	_total_ref+=weight;
	if(refe!=predict) {_errors+=weight; }
	else {++_exactMatch; ++_multiLabelAccuracy;}
	return refe!=predict;
}


void classificationReport::addMultilabel(const std::vector<uint>& ref,const std::vector<uint>& pred)
{
	//need sorted elements to use set_methods
	std::set<uint> reference(ref.begin(),ref.end());
	std::set<uint> predicted(pred.begin(),pred.end());
	
	++_nbSamples;		
	for(const auto r : reference)
		this->_reference[r]+=1;
	for(const auto r: predicted)
		this->_prediction[r]+=1;
	_total_pred+=predicted.size();
	_total_ref+=reference.size();


	std::set<uint> correct; //list of correctly recognized labels
	std::vector<uint> insertions; //insertions errors (predicted but not in ref)
	std::vector<uint> deletions; //deletion errors (in ref but not predicted)
	std::vector<uint> recoplusref;
	//labels well recognized
	set_intersection(predicted.begin(), predicted.end(), reference.begin(), reference.end(), std::inserter(correct,correct.end()));
	//insertions
	set_difference(predicted.begin(),predicted.end(), correct.begin(),correct.end(), std::back_inserter(insertions));
	//deletions
	set_difference(reference.begin(),reference.end(), correct.begin(),correct.end(), std::back_inserter(deletions));
	//for multilabel accuracy metric
	set_union(reference.begin(), reference.end(), predicted.begin(), predicted.end(), std::back_inserter(recoplusref));
	
	//labels well recognized
	for(const auto g: correct)
	 	_confusion.add(g,g);
	
	//virtual substitution: insertions or deletions are tranfered into substitution
	for(;!insertions.empty() && !deletions.empty();++_errors) //substitions errors
	{	
		_confusion.add(deletions.back(),insertions.back()); //a label deleted from ref is aligned with a label inserted in pred		
		deletions.pop_back();
		insertions.pop_back();
	}
	//add insertions and deletion errors that remains after virtual substitution
	_errors+=insertions.size()+deletions.size();


	if(correct.size() == predicted.size() && predicted.size()==reference.size())
		++_exactMatch;	

	_multiLabelAccuracy += static_cast<reel>(correct.size()) / static_cast<reel>(recoplusref.size());

}

void regressionReport::add(creel ref,creel predict)
{
	_target_predicted_value.push_back({ref,predict});
}

reel regressionReport::deviation() const
{
	creel moyenne=this->mean();
	return std::accumulate(_target_predicted_value.cbegin(), _target_predicted_value.cend(), static_cast<reel>(0), [=](creel acc, const auto& e) {return acc + pow(e.first - moyenne, 2); }) / _target_predicted_value.size();
}

reel regressionReport::mean() const
{
	return std::accumulate(_target_predicted_value.cbegin(), _target_predicted_value.cend(), static_cast<reel>(0), [](creel acc, const auto& e) {return acc + e.first; }) / _target_predicted_value.size();
}

void regressionReport::computeMetrics() const
{
	_mae=std::accumulate(_target_predicted_value.cbegin(), _target_predicted_value.cend(), static_cast<reel>(0), [](creel acc, const auto& x) {return acc + fabs(x.first-x.second); }) / _target_predicted_value.size();
	const reel mse=std::accumulate(_target_predicted_value.cbegin(), _target_predicted_value.cend(), static_cast<reel>(0), [](creel acc, const auto& x) {return acc + pow(x.first-x.second,2); }) / _target_predicted_value.size();

	
	const reel dev=this->deviation();
	_nmse=mse/dev;
	_rmse=sqrt(mse);
	_r2=1-mse/dev;

}

void classificationReport::computeMetrics() const
{
	this->_macroF1 = 0;
	this->_balancedAccuracy=0;
	reel pg = 0;
	_standartMetrics.resize(_reference.size() + 1);
	for(uint i = 0; i<_reference.size(); ++i)
	{
		if(_prediction[i]>0) _standartMetrics[i].precision = _confusion(i, i) / _prediction[i]; else _standartMetrics[i].precision = 1;
		if(_reference[i] > 0) _standartMetrics[i].recall = _confusion(i, i) / _reference[i]; else _standartMetrics[i].recall = 1;
		_standartMetrics[i].fmesure = fscore(_standartMetrics[i].precision, _standartMetrics[i].recall);
		_standartMetrics[i].err = _prediction[i] - _confusion(i, i);/*total de label a reconnu moins les biens reconnus donne les erreurs insertions*/

		if(_reference[i] - _prediction[i] > 0) _standartMetrics[i].err += (_reference[i] - _prediction[i]); //donc ici precision et recall sont bon mais le CER prend en compte le fait que l'on ne se prononce pas comme une deletion

		if(_reference[i] > 0) _standartMetrics[i].cer = _standartMetrics[i].err / _reference[i] ; else _standartMetrics[i].cer = _standartMetrics[i].err;

		pg += _confusion(i, i);

		_macroF1 += _standartMetrics[i].fmesure;
		_balancedAccuracy+=_standartMetrics[i].recall;
		
	}
	if(_total_pred > 0) _standartMetrics.back().precision = pg / _total_pred; else _standartMetrics.back().precision = 1;
	if(_total_ref > 0) _standartMetrics.back().recall = pg / _total_ref; else _standartMetrics.back().recall = 1;
	_standartMetrics.back().fmesure = fscore(_standartMetrics.back().precision, _standartMetrics.back().recall);
	_standartMetrics.back().err = this->_errors;
	if(_total_ref > 0) _standartMetrics.back().cer = _standartMetrics.back().err*1.0/ _total_ref; else _standartMetrics.back().cer = _standartMetrics.back().err;	

	_macroF1 /= _reference.size();//average of individual f1	
	_balancedAccuracy /= _reference.size();

}

string regressionReport::getAsText() const
{
	std::ostringstream out;
	out << "| "<<_target_predicted_value.size() << " examples\n";
	out << "| Mean Absolute Error          MAE =" << _mae << "\n";
	out << "| Root Mean Square Error       RMSE=" << _rmse << "\n";
	out << "| Normalized Mean Square Error NMSE=" << _nmse << "\n";
	out << "| Coefficient of Determination R2  =" << _r2 << "\n";
	return out.str();
}

string regressionReport::getAsHtml() const
{
	std::ostringstream out;
	out << "<tr><td colspan=3>"<< _target_predicted_value.size() << " examples</td></tr>\n";
	out << "<tr><td> Mean Absolute Error          </td><td>MAE =" << _mae << "</td></tr>\n";
	out << "<tr><td> Root Mean Square Error       </td><td>RMSE=" << _rmse << "</td></tr>\n";
	out << "<tr><td> Normalized Mean Square Error </td><td>NMSE=" << _nmse << "</td></tr>\n";
	out << "<tr><td> Coefficient of Determination </td><td>R2  =" << _r2 << "</td></tr>\n";
	return out.str();
}

string classificationReport::getAsHtml(std::vector<string> labels_name,const PrintOrder po,const bool interm) const
{
	this->computeMetrics();
	const auto& order=initLabelName(labels_name,_reference.size(),po);
	std::ostringstream html;
	html << "<html>\n<table CELLPADDING=2 align=center BORDER=2 BORDERCOLOR=black>\n";
	html << "<tr align=right><th> Label </th><th> HYP </td><th> REF </th><th> Correct </th><th> Error </th><th> Precision </th><th> Recall </th><th> F-Measure </th><th> Error Rate </th></tr>\n";
	if(interm)
	{
		for(const auto i: order)
			html << std::format("<tr align=right><td>{}</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td>{}</tr>\n", labels_name[i], _prediction[i], _reference[i], _confusion(i, i),_standartMetrics[i].asHtmlCells());
			//html << "<tr align=right><td>" << labels_name[i] << "</td><td>" << std::fixed << std::setprecision(0) << _prediction[i] << "</td><td>" << std::fixed << std::setprecision(0) << _reference[i] << "</td><td>" << _confusion(i, i) << "</td>"<<_standartMetrics[i].asHtmlCells()<<"</tr>\n";
	}
	html << std::format("<tr align=right><td>All</td><td>{:.0f}</td><td>{:.0f}</td><td>{:.0f}</td>{}</tr>\n",_total_pred,_total_ref, _confusion.nbCorrects(),_standartMetrics.back().asHtmlCells());
	//html << "<tr  align=right><td>" << "All </td><td>" << std::fixed << std::setprecision(0) << _total_pred << "</td><td>" << std::fixed << std::setprecision(0) << _total_ref << "</td><td>" << static_cast<uint>(_confusion.nbCorrects()) << "</td>"<<_standartMetrics.back().asHtmlCells()<<"</tr>\n";
	/*if(arg.get_runmode()==NUMERIQUE) 
	{	
		html << "<tr><td colspan=8>Mean deviation</td><td align=right>" << _confusion.compute_moyenne_ecart() << "</td></tr>" << endl;
	}*/
	html << "<tr><td colspan=8>Macro F1</td><td align=right>" << _macroF1*100 << "</td></tr>\n";
	html << "<tr><td colspan=8>Exact Match</td><td align=right>" << _exactMatch*100.0 / static_cast<reel>(_nbSamples) << "</td></tr>\n";
	html << "<tr><td colspan=8>Multi-Label Accuracy</td><td align=right>" << _multiLabelAccuracy*100.0 / static_cast<reel>(_nbSamples) << "</td></tr>\n";
	html << "<tr><td colspan=8>Balanced Accuracy</td><td align=right>" << _balancedAccuracy*100.0 << "</td></tr>\n";
	html << "<tr><td colspan=8>Hamming Loss </td><td align=right>" << _standartMetrics.back().err*100.0 / (static_cast<reel>(_nbSamples)*_reference.size()) << "</td></tr>\n";
	html << "</table>\n";
	html << "<br>\n"<<_confusion.getAsHtml(labels_name) << "\n";	
	html<<"</html>\n";
	return html.str();
}

std::vector<uint> classificationReport::orderByName(const std::vector<string>& l) const
{
	std::vector<uint> order(l.size());
	std::vector<std::pair<std::string,uint>> liste(l.size());
	transform(l.begin(),l.end(),liste.begin(),[i=0](const auto& s)mutable{  return std::make_pair(s,i++);});
	std::sort(liste.begin(),liste.end(),[](const auto& un,const auto& deux){return un.first<deux.first;});
	std::transform(liste.begin(),liste.end(),order.begin(),[](const auto& s){ return s.second;});
	return order;
}

std::vector<uint> classificationReport::orderByF1() const
{
	std::vector<uint> order(_reference.size());
	std::vector<std::pair<reel,uint>> liste(_reference.size());
	transform(_standartMetrics.begin(),_standartMetrics.end()-1,liste.begin(),[i=0](const auto& s)mutable{  return std::make_pair(s.fmesure,i++);});
	std::sort(liste.begin(),liste.end(),[](const auto& un,const auto& deux){return un.first>deux.first;});
	std::transform(liste.begin(),liste.end(),order.begin(),[](const auto& s){ return s.second;});
	return order;
}

std::vector<uint> classificationReport::initLabelName(std::vector<std::string>& l,cuint size,const PrintOrder po) const
{
	
	if(l.size() != size)
	{
		l.resize(size);
		for(uint i=0;i<l.size();++i)
			l[i]=std::to_string(i);
	}
	
	switch(po)
	{
		case PrintOrder::NAME:return orderByName(l);
		case PrintOrder::F1:return orderByF1();
		default: return orderByName(l);
	}

}

string classificationReport::getAsText(std::vector<string> labels_name,const PrintOrder po,const bool interm) const
{	
	this->computeMetrics();
	const auto& order=initLabelName(labels_name,_reference.size(),po);
	std::ostringstream out;
	asciitablestream ascii;
	ascii << "Label" << "Prediction" << "GroundTruth" << "Correct" <<"Error"<< "Precision" << "Recall" << "F1-score" << "ErrorRate"<< asciitablestream::endl;

	if(interm)
	{	
		for(const auto i: order)
		{
			ascii << labels_name[i] << static_cast<uint>(_prediction[i]) << static_cast<uint>(_reference[i]) << static_cast<uint>(_confusion(i, i)) <<  _standartMetrics[i].err << _standartMetrics[i].getVectorsOfMetrics() << asciitablestream::endl ;
		}
	}
	ascii << "All" <<static_cast<int>(_total_pred) << static_cast<int>(_total_ref) << static_cast<uint>(_confusion.nbCorrects()) << _standartMetrics.back().err << _standartMetrics.back().getVectorsOfMetrics() << asciitablestream::endl;
	out << ascii;
	
	out << std::setprecision(2)<<std::fixed;
	const reel f1_confidence_interval=1.96*std::sqrt(this->fscore()*(1.0-this->fscore())/_total_ref)*100;
	out << "| Micro F1             = " << this->fscore()*100<<"% ["<<(this->fscore()*100-f1_confidence_interval)<<","<<(this->fscore()*100+f1_confidence_interval)<<"] at 95%\n";
	out << "| Macro F1             = " << _macroF1*100 << "%\n";
	out << "| Exact Match          = " << _exactMatch*100.0 / static_cast<reel>(_nbSamples) << "%\n";
	out << "| Multi-Label Accuracy = " << _multiLabelAccuracy*100.0 / static_cast<reel>(_nbSamples) << "%\n";
	out << "| Balanced Accuracy    = " << _balancedAccuracy*100.0 << "%\n";
	out << "| Hamming Loss         = " << _standartMetrics.back().err*100.0 / (static_cast<reel>(_nbSamples)*_reference.size() )<< "%\n";

	//out << _confusion.getAsText();
	return out.str();
}


std::ostream& operator<<(std::ostream & o,const classificationReport& r)
{
	return o<<r.getAsText();
}
std::ostream& operator<<(std::ostream & o,const regressionReport& r)
{
	return o<<r.getAsText();
}

string classificationReport::color(reel score)
{
	std::ostringstream out;
	if(score== std::numeric_limits<reel>::quiet_NaN() || std::numeric_limits<reel>::signaling_NaN() == score ) return "FFFFFF";
	if(score>100) score=100;
	enum color{VERT,ROUGE} couleur=VERT;
	if(score>=50.0)
	{
		couleur=ROUGE;
		score-=50;
	}
	if(	couleur==ROUGE)
		score=255-(50-score);
	else
		score=255-score;
	

	if(couleur==ROUGE)
		out << std::hex << int(score);
	else
		out <<"00";
	if(couleur==VERT) 
		out << std::hex << int(score);
	else
		out << "00";
	out << "00";
	return out.str();
}



classificationReport& classificationReport::operator+=(const classificationReport& d)
{
	std::transform(_reference.cbegin(),_reference.cend(),d._reference.cbegin(),_reference.begin(), std::plus<reel>());
	std::transform(_prediction.cbegin(),_prediction.cend(),d._prediction.cbegin(),_prediction.begin(), std::plus<reel>());
	_confusion+=d._confusion;
	_total_ref+=d._total_ref;
	_total_pred+=d._total_pred;
	_multiLabelAccuracy+=d._multiLabelAccuracy;
	_balancedAccuracy+=d._balancedAccuracy;
	_exactMatch+=d._exactMatch;
	_nbSamples+=d._nbSamples;
	_errors+=d._errors;
	this->computeMetrics();
	return *this;
}

regressionReport& regressionReport::operator+=(const regressionReport& d)
{
	_target_predicted_value.insert(_target_predicted_value.end(),d._target_predicted_value.begin(),d._target_predicted_value.end());
	return *this;
}

void classificationReport::gain(creel un,creel deux,asciitablestream& tun,asciitablestream& tdeux,const bool relative,const bool biggerIsBetter)
{
	creel denominator=relative? (un+deux): 1;
	creel factor=relative? 100: 1;
	creel diff=(un+deux)>0? fabs(un-deux)*factor/denominator : 0;
	if(diff==0) { tun << "-"; tdeux<<"-";}
	else
	{
		if((un>deux && biggerIsBetter) || (un<deux && !biggerIsBetter))
		{
			tun<<diff;
			tdeux<<"-";
		}
		else
		{
			tdeux<<diff;
			tun<<"-";
		}
	}
	
}

std::string classificationReport::compar(const classificationReport& cr,const bool relative_difference,std::vector<string> labels_name,const PrintOrder po) const
{
	if(cr.nbClasses()!=this->nbClasses())
	{
		std::cerr << "Warning: reports differ in number of classes: can not compar them\n";
		return {"",""};
	}
	computeMetrics();
	cr.computeMetrics();

	auto order=initLabelName(labels_name,_reference.size(),po);
	labels_name.emplace_back("All");
	order.push_back(_standartMetrics.size()-1);

	asciitablestream un,deux;
	un << "Label"<<"Precision"<< "Recall"<<"F1"<<"CER"<<asciitablestream::endl;
	deux << "Precision"<< "Recall"<<"F1"<<"CER"<<asciitablestream::endl;
	for(cuint i: order)
	{
		un << labels_name[i];
		gain(precision(i),cr.precision(i),un,deux,relative_difference);
		gain(recall(i),cr.recall(i),un,deux,relative_difference);
		gain(fscore(i),cr.fscore(i),un,deux,relative_difference);
		gain(cer(i),cr.cer(i),un,deux,relative_difference,false);
		un<<asciitablestream::endl;
		deux<<asciitablestream::endl;
		
	}
	//explication gain
	std::vector<std::pair<uint,int>> gain1,gain2;
	const bool bestIsSys1=err()-cr.err()<0 ? true: false;
	//remove entry for global measure
	order.pop_back();
	for(cuint i: order)
	{
		//ici je ne suis pas satisfait car un système peux faire plus d'erreur mais prédire plus de ok
		//if(bestIsSys1 && err(i)<cr.err(i))
		//	gain.emplace_back(i,(cr.err(i)-err(i)));
		//if(!bestIsSys1 && err(i)>cr.err(i))
		//	gain.emplace_back(i,(err(i)-cr.err(i)));
		gain1.emplace_back(i,(_confusion(i,i)-err(i))-(cr._confusion(i,i)-cr.err(i)));
		gain2.emplace_back(i,(cr._confusion(i,i)-cr.err(i))-(_confusion(i,i)-err(i)));
		
	}
	std::sort(gain1.begin(),gain1.end(),[](const auto u,const auto d){return u.second>d.second;});	
	uint positive=std::find_if(gain1.begin(),gain1.end(),[](const auto u){return u.second<=0;})-gain1.begin();
	gain1.resize(std::min(10u,positive));

	std::sort(gain2.begin(),gain2.end(),[](const auto u,const auto d){return u.second>d.second;});	
	positive=std::find_if(gain2.begin(),gain2.end(),[](const auto u){return u.second<=0;})-gain2.begin();
	gain2.resize(std::min(10u,positive));

	const std::string& bestsys=bestIsSys1 ? "sys1": "sys2";
	const std::string& worstsys=bestIsSys1 ? "sys2": "sys1";
	asciitablestream expl;
	expl << "Label" << "correction of "+bestsys << asciitablestream::endl;
	for(const auto& e: gain1)
	{
		expl << labels_name[e.first] << "+"+std::to_string(e.second) << asciitablestream::endl;
	}
	asciitablestream explw;
	explw << "Label" << "correction of "+worstsys << asciitablestream::endl;
	for(const auto& e: gain2)
	{
		explw << labels_name[e.first] << "+"+std::to_string(e.second) << asciitablestream::endl;
	}

	return un.toStringBeside(deux,"\t","system1","system2")+"\n\n"+expl.toStringBeside(explw,"\t","Top 10 corrections of "+bestsys,"Top 10 corrections of "+worstsys);



}

}