	
#ifndef METRICS_H
#define METRICS_H
#include <algorithm>
#include <numeric>
#include <chrono>
#include <vector>
#include <set>
#include <format>
#include "Confusion.hpp"

namespace metrics
{


class classificationReport
{
public:
	enum PrintOrder {NAME,F1};
private:
	struct classificationStandardMetrics
	{
		uint err;
		reel precision;
		reel recall;
		reel fmesure;
		reel cer;
		void operator*=(creel w)
		{
			precision *= w;
			recall *= w;
			fmesure *= w;
			cer *= w;
		}
		std::vector<reel> getVectorsOfMetrics() const
		{
			return {precision*100,recall*100,fmesure*100,cer*100};
		}
		std::string asHtmlCells() const
		{
			return std::format("<td> {} </td><td> {:.2f} </td><td> {:.2f} </td><td> {:.2f} </td><td bgcolor=#{}> {:.2f} </td>",err,precision*100,recall*100,fmesure*100,color(cer*100),cer*100);			
		}


	};
	std::vector<reel> _reference; //frequency of each reference target label 
	std::vector<reel> _prediction; //frequency of each predicted label	
	reel _total_ref=0;
	reel _total_pred=0;
	uint _nbSamples=0;
	reel _errors=0;
	// confusion matrix
	Confusion _confusion;
	
	// standard metrics for each label + global
	mutable std::vector<classificationStandardMetrics> _standartMetrics;
	//global additional metrics
	mutable reel _exactMatch=0;
	mutable reel _multiLabelAccuracy=0;
	mutable reel _macroF1=0;
	mutable reel _balancedAccuracy=0;


	constexpr reel fscore(creel precision, creel recall) const{	if(precision + recall>0) return  2 * precision*recall / (precision + recall); else	return 0;}	
	static string color(reel score);
	void computeMetrics() const;
	static void gain(creel,creel,asciitablestream& un,asciitablestream& deux,const bool relative=true,const bool biggerIsBetter=true);
	std::vector<uint> initLabelName(std::vector<std::string>& l,cuint size,const PrintOrder o) const;
	std::vector<uint> orderByName(const std::vector<string>& labels_name) const;
	std::vector<uint> orderByF1() const;
public:
	explicit classificationReport(cuint nbLabels);	
	classificationReport& operator+=(const classificationReport&);
	bool add(cuint refe,cuint pred,creel weight=1);
	template<class Collection>
	void operator()(const Collection& refe,const Collection& pred);
	void addMultilabel(const std::vector<uint>& refe,const std::vector<uint>& pred);
	reel errors() const {  return _errors; }
	reel cer() const {  return _standartMetrics.back().cer; }
	reel err() const {  return _standartMetrics.back().err; }
	reel fscore() const { return _standartMetrics.back().fmesure; }
	string getAsText(std::vector<string> labels_name={},const PrintOrder po=PrintOrder::NAME,const bool interm=true) const;
	string getAsHtml(std::vector<string> labels_name={},const PrintOrder po=PrintOrder::NAME,const bool interm=true) const;
	uint nbClasses() const {return _standartMetrics.size()-1;}
	//return number of errors
	reel err(cuint i) const { return _standartMetrics[i].err; }
	reel cer(cuint i) const { return _standartMetrics[i].cer; }
	reel precision(cuint i) const { return _standartMetrics[i].precision; }
	reel recall(cuint i) const { return _standartMetrics[i].recall; }
	reel fscore(cuint i) const { return _standartMetrics[i].fmesure; }
	const matrix& getConfusionMatrix() const {return _confusion.getConfusionMatrix();}
	//compar deux reports and outputs 2 tables resuming on what each one is better
	std::string compar(const classificationReport& cr,const bool relative_difference=true,std::vector<string> labels_name={},const PrintOrder po=PrintOrder::NAME) const;
};

template<class Collection>
void classificationReport::operator()(const Collection& refe,const Collection& pred)
{
	if(refe.size()!=pred.size())
		throw std::length_error("reference and prediction sizes differ");
	auto irefe=refe.begin();
	auto ipred=pred.begin();
	for(;irefe!=refe.end();++irefe,++ipred)
	{
		this->add(*irefe,*ipred);
	}
	this->computeMetrics();
}

class regressionReport
{
	std::vector<std::pair<reel,reel>> _target_predicted_value;
	
	//metrics
	mutable reel _mae;
	mutable reel _rmse;
	mutable reel _nmse;
	mutable reel _r2;
	
	reel deviation() const;
	reel mean() const;
	void computeMetrics() const;	
public:
	void add(creel ref, creel predict);		
	template<class Collection>
	void operator()(const Collection& refe,const Collection& pred);	
	regressionReport& operator+=(const regressionReport& d);		
	reel mae() const noexcept {return _mae;}
	reel rmse() const noexcept{return _rmse;}
	reel nmse() const noexcept {return _nmse;}
	reel r2() const noexcept {return _r2;}
	string getAsText() const;
	string getAsHtml() const;
};

template<class Collection>
void regressionReport::operator()(const Collection& refe,const Collection& pred)
{
	if(refe.size()!=pred.size())
		throw std::length_error("reference and prediction sizes differ");
	auto irefe=refe.begin();
	auto ipred=pred.begin();
	for(;irefe!=refe.end();++irefe,++ipred)
	{
		this->add(*irefe,*ipred);
	}
	this->computeMetrics();
}

std::ostream& operator<<(std::ostream & o,const classificationReport& r);
std::ostream& operator<<(std::ostream & o,const regressionReport& r);

}

#endif


