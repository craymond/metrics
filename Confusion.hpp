	
#ifndef CONFUSION_H
#define CONFUSION_H
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <numeric>
#include <cmath>
#include <format>
#include "asciitablestream.hpp"

namespace metrics
{

using reel=double;
using creel = const reel;
using std::string;
using uint=unsigned int;
using cuint = const unsigned int;
using matrix=std::vector<std::vector<reel>>;

class Confusion
{
	matrix confusion;
	reel somme=0;

	
	string color(cuint un,cuint deux) const;
	void format_labelnames(std::vector<string>& label_names) const;
public:
	Confusion(cuint nbClasses=2,creel init=0);
	void add(cuint i, cuint j);
	Confusion& operator+=(const Confusion&);
	reel operator()(cuint i, cuint j) const {return confusion[i][j];}
	reel& operator()(cuint i, cuint j){return confusion[i][j];}
	string getAsHtml(std::vector<std::string> labels_name={}) const;
	string getAsText(std::vector<std::string> labels_name={}) const;
	const matrix& getConfusionMatrix() const {return confusion;}
	reel nbCorrects() const;
	void reset();
};

}

#endif

