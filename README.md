# C++ classes to evaluate classification or regression systems

## features

- compute regression or classification metrics
- outputs metrics for each target labels
- works with multiclass/multilabel problems
- can compare two reports from 2 systems

## example

```cpp
#include "Metrics.hpp"
#include <list>

int main(int argc,char* argv[])
{
    asciitablestream::set_style(asciitablestream::Style::DOUBLE);
    //lists of reference labels and prediction (may be interpreted as classification or regression values)
    std::vector<uint> references={0,0,1,0,2,0}; //works with any container, not only list
    std::vector<uint> predictions={0,0,1,0,2,1};

    std::cout << "\nregression report\n";
    metrics::regressionReport rreport;
    rreport(references,predictions);
    std::cout << rreport;

    std::cout << "\nclassification report\n";
    metrics::classificationReport report(3);
    report(references,predictions);
    std::cout << report.getAsText({"zero","one","two"});
    
    std::cout << "\nMultilabels\n";
    std::vector<std::vector<uint>> ref={{0,1,2},{0,1,2,3}};
    std::vector<std::vector<uint>> pred={{0,1,2},{0,1,2}};
    metrics::classificationReport mreport(4);
    for(uint i=0;i<ref.size();++i)
        mreport.addMultilabel(ref[i],pred[i]);
    std::cout << mreport;
  
    std::cout << "\nsystems Comparison\n";
    std::vector<uint> predictions2={0,0,1,0,1,0};
    metrics::classificationReport report2(3);
    report2(references,predictions2);
    std::cout << report2.getAsText({"zero","one","two"});
    const auto res=report.compar(report2,true,{"zero","one","two"});
    std::cout<<"\nSystem1 vs System2\n"<< res.first;
    std::cout<<"\nSystem2 vs System1\n"<< res.second;
  
    return 0;
}
}
```

## output

```console

regression report
| 6 examples
| Mean Absolute Error          MAE =0.166667
| Root Mean Square Error       RMSE=0.408248
| Normalized Mean Square Error NMSE=0.285714
| Coefficient of Determination R2  =0.714286

classification report
╔═══════╦════════════╦═════════════╦═════════╦═══════╦═══════════╦════════╦══════════╦═══════════╗
║ Label ║ Prediction ║ GroundTruth ║ Correct ║ Error ║ Precision ║ Recall ║ F1-score ║ ErrorRate ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   one ║          2 ║           1 ║       1 ║     1 ║     50.00 ║ 100.00 ║    66.67 ║    100.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   two ║          1 ║           1 ║       1 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║  zero ║          3 ║           4 ║       3 ║     1 ║    100.00 ║  75.00 ║    85.71 ║     25.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   All ║          6 ║           6 ║       5 ║     1 ║     83.33 ║  83.33 ║    83.33 ║     16.67 ║
╚═══════╩════════════╩═════════════╩═════════╩═══════╩═══════════╩════════╩══════════╩═══════════╝
| Micro F1             = 83.33% [53.51,113.15] at 95%
| Macro F1             = 84.13%
| Exact Match          = 83.33%
| Multi-Label Accuracy = 83.33%
| Hamming Loss         = 5.56%

Multilabels
╔═══════╦════════════╦═════════════╦═════════╦═══════╦═══════════╦════════╦══════════╦═══════════╗
║ Label ║ Prediction ║ GroundTruth ║ Correct ║ Error ║ Precision ║ Recall ║ F1-score ║ ErrorRate ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║     0 ║          2 ║           2 ║       2 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║     1 ║          2 ║           2 ║       2 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║     2 ║          2 ║           2 ║       2 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║     3 ║          0 ║           1 ║       0 ║     1 ║    100.00 ║   0.00 ║     0.00 ║    100.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   All ║          6 ║           7 ║       6 ║     1 ║    100.00 ║  85.71 ║    92.31 ║     14.29 ║
╚═══════╩════════════╩═════════════╩═════════╩═══════╩═══════════╩════════╩══════════╩═══════════╝
| Micro F1             = 92.31% [72.57,112.05] at 95%
| Macro F1             = 75.00%
| Exact Match          = 50.00%
| Multi-Label Accuracy = 87.50%
| Hamming Loss         = 12.50%

systems Comparison
╔═══════╦════════════╦═════════════╦═════════╦═══════╦═══════════╦════════╦══════════╦═══════════╗
║ Label ║ Prediction ║ GroundTruth ║ Correct ║ Error ║ Precision ║ Recall ║ F1-score ║ ErrorRate ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   one ║          2 ║           1 ║       1 ║     1 ║     50.00 ║ 100.00 ║    66.67 ║    100.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   two ║          1 ║           1 ║       1 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║  zero ║          3 ║           4 ║       3 ║     1 ║    100.00 ║  75.00 ║    85.71 ║     25.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   All ║          6 ║           6 ║       5 ║     1 ║     83.33 ║  83.33 ║    83.33 ║     16.67 ║
╚═══════╩════════════╩═════════════╩═════════╩═══════╩═══════════╩════════╩══════════╩═══════════╝
| Micro F1             = 83.33% [53.51,113.15] at 95%
| Macro F1             = 84.13%
| Exact Match          = 83.33%
| Multi-Label Accuracy = 83.33%
| Hamming Loss         = 5.56%
╔═══════╦════════════╦═════════════╦═════════╦═══════╦═══════════╦════════╦══════════╦═══════════╗
║ Label ║ Prediction ║ GroundTruth ║ Correct ║ Error ║ Precision ║ Recall ║ F1-score ║ ErrorRate ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   one ║          2 ║           1 ║       1 ║     1 ║     50.00 ║ 100.00 ║    66.67 ║    100.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   two ║          0 ║           1 ║       0 ║     1 ║    100.00 ║   0.00 ║     0.00 ║    100.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║  zero ║          4 ║           4 ║       4 ║     0 ║    100.00 ║ 100.00 ║   100.00 ║      0.00 ║
╠═══════╬════════════╬═════════════╬═════════╬═══════╬═══════════╬════════╬══════════╬═══════════╣
║   All ║          6 ║           6 ║       5 ║     1 ║     83.33 ║  83.33 ║    83.33 ║     16.67 ║
╚═══════╩════════════╩═════════════╩═════════╩═══════╩═══════════╩════════╩══════════╩═══════════╝
| Micro F1             = 83.33% [53.51,113.15] at 95%
| Macro F1             = 55.56%
| Exact Match          = 83.33%
| Multi-Label Accuracy = 83.33%
| Hamming Loss         = 5.56%

System1 vs System2
╔═══════╦═══════════╦════════╦════════╦════════╗	╔═══════════╦════════╦══════╦════════╗
║ Label ║ Precision ║ Recall ║     F1 ║    CER ║	║ Precision ║ Recall ║   F1 ║    CER ║
╠═══════╬═══════════╬════════╬════════╬════════╣	╠═══════════╬════════╬══════╬════════╣
║   one ║         - ║      - ║      - ║      - ║	║         - ║      - ║    - ║      - ║
╠═══════╬═══════════╬════════╬════════╬════════╣	╠═══════════╬════════╬══════╬════════╣
║   two ║         - ║ 100.00 ║ 100.00 ║ 100.00 ║	║         - ║      - ║    - ║      - ║
╠═══════╬═══════════╬════════╬════════╬════════╣	╠═══════════╬════════╬══════╬════════╣
║  zero ║         - ║      - ║      - ║      - ║	║         - ║  14.29 ║ 7.69 ║ 100.00 ║
╠═══════╬═══════════╬════════╬════════╬════════╣	╠═══════════╬════════╬══════╬════════╣
║   All ║         - ║      - ║      - ║      - ║	║         - ║      - ║    - ║      - ║
╚═══════╩═══════════╩════════╩════════╩════════╝	╚═══════════╩════════╩══════╩════════╝

```
