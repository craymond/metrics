#include "Metrics.hpp"
#include <list>

int main(int argc,char* argv[])
{
   
    asciitablestream::set_style(asciitablestream::Style::DOUBLE);
    //lists of reference labels and prediction (may be interpreted as classification or regression values)
    std::vector<metrics::uint> references={0,0,1,0,2,0}; //works with any container, not only list
    std::vector<metrics::uint> predictions={0,0,1,0,2,1};

    std::cout << "\nregression report\n";
    metrics::regressionReport rreport;
    rreport(references,predictions);
    std::cout << rreport;

    std::cout << "\nclassification report\n";
    metrics::classificationReport report(3);
    report(references,predictions);
    std::cout << report.getAsText({"0-zero","1-one","2-two"});
    
    std::cout << "\nMultilabels\n";
    std::vector<std::vector<metrics::uint>> ref={{0,1,2},{0,1,2,3}};
    std::vector<std::vector<metrics::uint>> pred={{0,1,2},{0,1,2}};
    metrics::classificationReport mreport(4);
    for(metrics::uint i=0;i<ref.size();++i)
        mreport.addMultilabel(ref[i],pred[i]);
    std::cout << mreport;
  
    std::cout << "\nsystems Comparison\n";
    //std::vector<metrics::uint> references={0,0,1,0,2,0}; //works with any container, not only list
    //std::vector<metrics::uint> predictions={0,0,1,0,2,1};
    std::vector<metrics::uint> predictions2={0,0,1,0,1,0};
    metrics::classificationReport report2(3);
    report2(references,predictions2);
    std::cout << report.getAsText({"zero","one","two"},metrics::classificationReport::PrintOrder::NAME);
    std::cout << report2.getAsText({"zero","one","two"},metrics::classificationReport::PrintOrder::NAME);
    std::cerr << report2.getAsHtml({"zero","one","two"},metrics::classificationReport::PrintOrder::NAME);
    const auto res=report.compar(report2,true,{"zero","one","two"});
    std::cout<<"\nSystem1 vs System2\n"<< res;
    //report+=report;
    //std::cout << report
    return 0;
}